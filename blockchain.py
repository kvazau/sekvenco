# blockchain.py

import hashlib
import fib
import time


class Block(object):
    _data: bytes
    _hash: bytes
    _index: int
    _nonce: (int, int)
    _prev_hash: bytes
    _prev_nonce: int
    _timestamp: float

    def __init__(self, index: int, prev_hash: bytes, prev_nonce: (int, int), data: bytes):
        self._data = data
        self._hash = None
        self._index = index
        self._nonce = None
        self._prev_hash = prev_hash
        self._prev_nonce = prev_nonce
        self._timestamp = time.time()

    @property
    def data(self) -> bytes:
        return self._data

    @property
    def dump(self) -> dict:
        return {
            "data": self.data,
            "index": self.index,
            "hash": self.hash,
            "nonce": self.nonce,
            "prev_hash": self.prev_hash,
            "prev_nonce": self.prev_nonce,
            "timestamp": self.timestamp
        }

    @property
    def index(self) -> int:
        return self._index

    @property
    def hash(self) -> bytes:
        """
        Computes bytestring representing totality of this block.

        :return bytes:
        """
        # Create an unhashed sequence of bytes representing block.
        hash_input = bytes(
            str([
                self.data,
                self.index,
                self.nonce,
                self.prev_hash,
                self.timestamp
            ]),
            "UTF-8"
        )

        # Calculate hash of byte sequence.
        self._hash = hashlib.sha256(hash_input).digest()
        return self._hash

    @property
    def nonce(self) -> int:
        if self._nonce is None:
            self._nonce = (
                self._prev_nonce[1],
                fib.next_fib(self._prev_nonce)
            )

        return self._nonce

    @property
    def prev_hash(self) -> bytes:
        return self._prev_hash

    @property
    def prev_nonce(self) -> int:
        return self._prev_nonce

    @property
    def timestamp(self) -> float:
        return self._timestamp


class Blockchain(object):
    _blocks: [Block]

    def __init__(self, initial_data: bytes):
        self._blocks = [Block(index=0, prev_hash=b"", prev_nonce=(1,1), data=initial_data)]

    @property
    def blocks(self):
        return self._blocks

    def mine(self, data: bytes) -> Block:
        prev_block = self.blocks[-1]
        new_block = Block(
            index=prev_block.index + 1,
            prev_hash=prev_block.hash,
            prev_nonce=prev_block.nonce,
            data=data
        )

        self._blocks.append(new_block)
        return new_block

    @property
    def dump(self) -> [dict]:
        return [block.dump for block in self.blocks]
