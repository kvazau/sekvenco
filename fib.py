# fib.py


def next_fib(prev_fibs: (int, int)) -> int:
    return sum(prev_fibs)